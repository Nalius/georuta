package cl.boxapp.geoasistencia;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.BoolRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cl.boxapp.geoasistencia.classes.Auxliar;
import cl.boxapp.geoasistencia.classes.DBhelper;
import cl.boxapp.geoasistencia.classes.Usuario;

import static android.R.attr.data;
import static android.R.attr.start;


public class FrRegistroUsuario extends Fragment {

    Button btnRegistrarme;


    AppCompatEditText txtRut,txtNombres,txtEmail,txtApellidoPaterno,txtApellidoMaterno;


    public FrRegistroUsuario() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fr_registro_usuario, container, false);
        eventos(v);
        return v;
    }

    private void eventos(View fr) {
        txtRut = (AppCompatEditText)fr.findViewById(R.id.txtRut);
        txtNombres = (AppCompatEditText)fr.findViewById(R.id.txtNombres);
        txtEmail = (AppCompatEditText)fr.findViewById(R.id.txtEmail);
        txtApellidoMaterno = (AppCompatEditText)fr.findViewById(R.id.txtApellidoMaterno);
        txtApellidoPaterno = (AppCompatEditText)fr.findViewById(R.id.txtApellidoPaterno);

        txtRut.setText("40.106.048-0");
        txtNombres.setText("Brayyan Alberto");
        txtEmail.setText("bayomc@gmail.com ");
        txtApellidoMaterno.setText("Yanes");
        txtApellidoPaterno.setText("Ochoa");

        btnRegistrarme = (Button)fr.findViewById(R.id.btnRegistrarme);
        btnRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtRut.setText(formatearRut(txtRut.getText().toString().trim()));
                String rut = txtRut.getText().toString().trim();
                String nombres = txtNombres.getText().toString().trim();
                String email = txtEmail.getText().toString().trim();
                String apellido_paterno = txtApellidoPaterno.getText().toString().trim();
                String apellido_materno = txtApellidoMaterno.getText().toString().trim();


                if(!rut.isEmpty()){
                    if(Auxliar.validarRut(rut)){
                        if(!nombres.isEmpty()){
                            if(!email.isEmpty()){
                                if(Auxliar.isValidEmail(email)){
                                    if(!apellido_paterno.isEmpty()){
                                        if(!apellido_materno.isEmpty()){

                                            TelephonyManager telephonyManager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);

                                            Usuario u = new Usuario();
                                            u.setRut(rut);
                                            u.setCorreo(email);
                                            u.setNombres(nombres);
                                            u.setApellido_Paterno(apellido_paterno);
                                            u.setApellido_Materno(apellido_materno);
                                            u.setImei(telephonyManager.getDeviceId());
                                            u.setId_empresa(Auxliar.id_empresa);
                                            u.setEstado(0);

                                            if(Auxliar.networkConnectivity(getActivity())){
                                                new AsyncRegistrarUsuario().execute(u);
                                            }else{
                                                Toast.makeText(getActivity(),"Sin Conexion a internet",Toast.LENGTH_LONG).show();
                                            }

                                        }else{
                                            Toast.makeText(getActivity(), "El Apellido Materno es obligatorio", Toast.LENGTH_SHORT).show();
                                        }
                                    }else{
                                        Toast.makeText(getActivity(), "El Apellido Paterno es obligatorio", Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    Toast.makeText(getActivity(), "El Email no es válido", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getActivity(), "El Email es obligatorio", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(getActivity(), "Los Nombres son obligatorio", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getActivity(), "El RUT no es válido", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "El RUT es obligatorio", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private String formatearRut(String rut){
        String n_rut = "";
        if(!rut.isEmpty()){
            if(rut.contains(".") && rut.contains("-")){
                n_rut = rut;
            }else if(rut.contains("-")){
                String[] aux = rut.split("-");
                rut = aux[0]+aux[1];
                if(rut.length() == 8){
                    n_rut = rut.charAt(0) + "." + rut.charAt(1) + rut.charAt(2) + rut.charAt(3) + "."  + rut.charAt(4) + rut.charAt(5) + rut.charAt(6) + "-" + rut.charAt(7);
                }else{
                    n_rut = rut.charAt(0) + "" + rut.charAt(1)+"." + rut.charAt(2) + "" +rut.charAt(3) +"" + rut.charAt(4) + "."  + rut.charAt(5) +"" + rut.charAt(6) + "" +rut.charAt(7) + "-" + rut.charAt(8);
                }
            }else{
                if(rut.length() == 8){
                    n_rut = rut.charAt(0) + "." + rut.charAt(1) + rut.charAt(2) + rut.charAt(3) + "."  + rut.charAt(4) + rut.charAt(5) + rut.charAt(6) + "-" + rut.charAt(7);
                }else{
                    n_rut = rut.charAt(0) +"" + rut.charAt(1)+"." + rut.charAt(2) +"" + rut.charAt(3) +"" + rut.charAt(4) + "."  + rut.charAt(5) +"" + rut.charAt(6) +"" + rut.charAt(7) + "-" + rut.charAt(8);
                }
            }
        }
        return n_rut;
    }

    public class AsyncRegistrarUsuario extends AsyncTask<Usuario,Void,JSONObject> {
        String url = Auxliar._SERVER;
        ProgressDialog pd;
        Boolean success = false;

        @Override
        protected void onPreExecute() {
            pd = ProgressDialog.show(getActivity(),"Registro","Espera un poco, te estamos registrando...");
        }

        @Override
        protected JSONObject doInBackground(Usuario... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url+"users");
            JSONObject jsonObject = new JSONObject();
            try {
                String json = "";
                jsonObject.put("rut", params[0].getRut());
                jsonObject.put("name", params[0].getNombres());
                jsonObject.put("first_lastname", params[0].getApellido_Paterno());
                jsonObject.put("second_lastname", params[0].getApellido_Materno());
                jsonObject.put("email", params[0].getCorreo());
                jsonObject.put("active", 0);
                jsonObject.put("imei", params[0].getImei());
                jsonObject.put("company_id",params[0].getId_empresa());
                json = jsonObject.toString();
                Log.e("Json",json);
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                HttpResponse response = httpclient.execute(httpPost);

                if(response!=null){

                    String result = EntityUtils.toString(response.getEntity());

                    jsonObject = new JSONObject(result);
                    Log.e("JSON",jsonObject.toString());

                    success = jsonObject.getBoolean("success");
                    Log.e("SUCCESS",String.valueOf(success));

                   if(success){
                       int id = 0;

                       JSONObject jdataObj = jsonObject.getJSONObject("data");
                       id = jdataObj.getInt("id");

                       DBhelper dbh = new DBhelper(getActivity());
                       SQLiteDatabase bd = dbh.getWritableDatabase();

                       ContentValues cv = new ContentValues();

                       cv.put("id_user",id);
                       cv.put("rut",params[0].getRut());
                       cv.put("name",params[0].getNombres());
                       cv.put("first_lastname",params[0].getApellido_Paterno());
                       cv.put("second_lastname",params[0].getApellido_Materno());
                       cv.put("email",params[0].getCorreo());
                       cv.put("active",0);
                       cv.put("imei",params[0].getImei());
                       cv.put("id_office",Auxliar.id_empresa);

                       long i = bd.insert("Usuario",null,cv);
                       Log.e("INSERT","-------------"+i+"---------");

                       bd.close();


                       /*
                       Auxliar.id_usuario = String .valueOf(id);
                       Auxliar.rut_usurio = params[0].getRut();
                       Auxliar.nombre_usuario = params[0].getApellido_Paterno();
                       Auxliar.email_usuario = params[0].getCorreo();
                       */

                       Auxliar.CURRENT_USER.setId(String.valueOf(id));
                       Auxliar.CURRENT_USER.setRut(params[0].getRut());
                       Auxliar.CURRENT_USER.setCorreo(params[0].getCorreo());
                       Auxliar.CURRENT_USER.setNombres(params[0].getNombres());
                       Auxliar.CURRENT_USER.setApellido_Paterno(params[0].getApellido_Paterno());

                    }else{
                        jsonObject = null;
                    }
                }else{
                    jsonObject = null;
                }

            } catch (ClientProtocolException e) {
                Log.e("ClientProtocolException",e.getMessage());
            } catch (IOException e) {
                Log.e("IOException",e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JSONException",e.getMessage());
            }

            return jsonObject;

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(jsonObject!=null ){
                if (success){
                    Toast.makeText(getActivity(), "Datos del usuario cargados", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(getActivity(),Panel.class);
                    startActivity(i);
                }else{
                    pd.dismiss();
                    Toast.makeText(getActivity(),"Error al registrar usuario",Toast.LENGTH_LONG).show();
                }
            }else{
                pd.dismiss();
                Toast.makeText(getActivity(),"Error al registrar usuario",Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }
}
