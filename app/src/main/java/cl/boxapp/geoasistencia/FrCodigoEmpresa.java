package cl.boxapp.geoasistencia;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cl.boxapp.geoasistencia.classes.Auxliar;
import cl.boxapp.geoasistencia.classes.DBhelper;
import cl.boxapp.geoasistencia.classes.Ruta;
import cl.boxapp.geoasistencia.classes.Usuario;


public class FrCodigoEmpresa extends Fragment {

    EditText txtCodigo;
    ImageButton btnConfirmar;


    public FrCodigoEmpresa() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fr_codigo_empresa, container, false);
        eventos(v);
        return v;
    }

    private void eventos(View fr) {
        txtCodigo = (EditText)fr.findViewById(R.id.txtCodigoEmpresa);
        btnConfirmar = (ImageButton)fr.findViewById(R.id.btnConfirmar);


        txtCodigo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("Count","" + s.length());
                if(s.length() == 4){
                    btnConfirmar.setVisibility(View.VISIBLE);

                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }else{
                    btnConfirmar.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Auxliar.id_autorizacion = txtCodigo.getText().toString();
                if(Auxliar.networkConnectivity(getActivity())){
                    new AsyncVerificarCodigo().execute(Auxliar.id_autorizacion);
                }else{
                    Toast.makeText(getActivity(),"Sin Conexion a internet",Toast.LENGTH_LONG).show();
                }
                TelephonyManager telephonyManager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                Log.e("AUTORIZACION",Auxliar.id_autorizacion);
                Log.e("IMEI",telephonyManager.getDeviceId());


            }
        });

    }

    public class AsyncVerificarCodigo extends AsyncTask<String,Void,JSONObject> {
        String url = Auxliar._SERVER;
        ProgressDialog pd;
        Boolean done = false;

        @Override
        protected void onPreExecute() {
            pd = ProgressDialog.show(getActivity(),"Registro","Espera un poco, te estamos registrando...");
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            //Log.e("PARAM",params[0]);
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpPost = new HttpGet(url+"companies/code"+params[0]);
            Log.e("URL",url+"companies/code"+params[0]);
            JSONObject jsonObject = null;
            try {
                HttpResponse response = httpclient.execute(httpPost);

                if(response!=null){
                    String result = EntityUtils.toString(response.getEntity());

                    jsonObject = new JSONObject(result);
                    Log.e("ERROR",jsonObject.toString());
                    Log.e("PARAM",params[0]);

                    done = jsonObject.getBoolean("success") ;


                    if(done){
                        JSONArray jsonData = jsonObject.getJSONArray("data");
                        JSONObject Jcursor = jsonData.getJSONObject(0);
                        Auxliar.id_empresa = String.valueOf(Jcursor.getInt("id"));

                    }else{
                        jsonObject = null;
                    }
                }

            } catch (ClientProtocolException e) {

            } catch (IOException e) {

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject;

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(jsonObject!=null ){
                if(done){
                    Toast.makeText(getActivity(), "Empresa Autorizada", Toast.LENGTH_SHORT).show();
                    FragmentManager fr = getActivity().getSupportFragmentManager();
                    fr.beginTransaction().replace(R.id.micontent,new FrRegistroUsuario()).commit();
                }
            }else{
                pd.dismiss();
                Toast.makeText(getActivity(),"Error al buscar empresa",Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }


}
