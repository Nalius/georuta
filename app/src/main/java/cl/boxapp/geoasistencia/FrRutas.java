package cl.boxapp.geoasistencia;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.icu.text.LocaleDisplayNames;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import cl.boxapp.geoasistencia.classes.Auxliar;
import cl.boxapp.geoasistencia.classes.GeoLocation;
import cl.boxapp.geoasistencia.classes.Ruta;
import cl.boxapp.geoasistencia.classes.Usuario;


public class FrRutas extends Fragment {

    ListRutas listRutas;
    RecyclerView rvLista;
    FloatingActionButton btnFab;
    ListRutas lr;

    public FrRutas() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_fr_rutas, container, false);

        rvLista = (RecyclerView)view.findViewById(R.id.rvLista);
        rvLista.setHasFixedSize(true);

        lr = new ListRutas(new ArrayList<Ruta>(),getActivity());

        rvLista.setAdapter(lr);

        rvLista.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));

        if (Auxliar.networkConnectivity(getActivity())){
            new AsyncGetTracks().execute(Auxliar.CURRENT_USER);
        }else{
            Toast.makeText(getActivity(),"Sin Conexion a internet",Toast.LENGTH_LONG).show();
        }

        btnFab = (FloatingActionButton)view.findViewById(R.id.fab);

        btnFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Auxliar.networkConnectivity(getActivity())){
                    new AsyncGetTracks().execute(Auxliar.CURRENT_USER);
                }else{
                    Toast.makeText(getActivity(),"Sin Conexion a internet",Toast.LENGTH_LONG).show();
                }
            }
        });


        return view;
    }


    public class AsyncGetTracks extends AsyncTask<Usuario,Void,ArrayList<Ruta>> {
        String url = Auxliar._SERVER;
        ProgressDialog pd;
        boolean success = false;


        @Override
        protected void onPreExecute() {
            pd = ProgressDialog.show(getActivity(),"Rutas","Espera un poco, cargando tus rutas...");
        }

        @Override
        protected ArrayList<Ruta> doInBackground(Usuario... params) {

            HttpClient httpclient = new DefaultHttpClient();

            HttpGet httpPost = new HttpGet(url+"users/"+params[0].getId()+"/tracks");

            ArrayList<Ruta> alRuta = new ArrayList<>();

            try {

                HttpResponse response = httpclient.execute(httpPost);

                if(response!=null){

                    String result = EntityUtils.toString(response.getEntity());

                    JSONObject Jsuccess = new JSONObject(result);

                    if(Jsuccess.length() > 0) {
                        success = Jsuccess.optBoolean("success");
                        if (success) {

                            JSONArray jdata = Jsuccess.getJSONArray("data");

                            JSONObject jdata2= jdata.getJSONObject(0);

                            JSONArray jdataTracks = jdata2.getJSONArray("tracks");

                            for (int i = 0; i < jdataTracks.length(); i++) {

                                JSONObject Jcursor = jdataTracks.getJSONObject(i);

                                Log.e("Cursor",Jcursor.toString());

                                Ruta ru = new Ruta();
                                ru.setId(Jcursor.getInt("id"));
                                ru.setFecha_hora(Auxliar.getFormatoFecha(Jcursor.getString("date")));
                                ru.setDetalle(Jcursor.getString("detail"));
                                ru.setComentarios(Jcursor.getString("comment"));
                                ru.setLatitud(Jcursor.getDouble("latitude"));
                                ru.setLongitud(Jcursor.getDouble("longitude"));
                                ru.setDescripcion(Jcursor.getString("description"));
                                alRuta.add(ru);
                            }
                        }
                    }
                }

            } catch (ClientProtocolException e) {

            } catch (IOException e) {

            } catch (JSONException e) {
                Log.e("JSONError",e.getMessage());
                e.printStackTrace();
            }

            return alRuta;

        }

        @Override
        protected void onPostExecute(ArrayList<Ruta> alRuta) {
            if (alRuta.size() > 0){
                boolean sw = false;
                while (!sw) {
                    if (GeoLocation.MyLocation != null) {
                        int ubicaciones = alRuta.size();
                        sw = true;
                        lr.setData(alRuta);
                        pd.dismiss();
                        Snackbar.make(getView(), "Actualizadas "+ ubicaciones+" ubicaciones", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }else{
                pd.dismiss();
                Snackbar.make(getView(),"Sin Rutas Para " + Auxliar.CURRENT_USER.getNombres(),Snackbar.LENGTH_SHORT).show();
            }
            Log.e("Lista",alRuta.size()+"");

        }
    }
}
