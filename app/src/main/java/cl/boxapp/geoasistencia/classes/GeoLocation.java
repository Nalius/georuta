package cl.boxapp.geoasistencia.classes;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.zip.CheckedOutputStream;

/**
 * Created by Nalius on 31-05-2017.
 */

public final class GeoLocation {

    public static Location RutaLocation;
    public static Location MyLocation;


    public static String showLocation(Location loc, Context context){
        String direccion = "";
        if (loc != null){
            if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
                try {
                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                    List<Address> list = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
                    if (!list.isEmpty()) {
                        Address address = list.get(0);
                        direccion = address.getAddressLine(0).toString();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return direccion;
    }


}
