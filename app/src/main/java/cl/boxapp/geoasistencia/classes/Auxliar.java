package cl.boxapp.geoasistencia.classes;

import android.content.Context;
import android.icu.text.DateFormat;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Nalius on 14-05-2017.
 */

public final class Auxliar {

    public static String _SERVER = "http://boxapp.cl/geoapi/public/api/";

    public static String id_empresa;
    public static String id_autorizacion;
    public static final Usuario CURRENT_USER =  new Usuario();
    public static Ruta CURRENT_ROUTE = new Ruta();
    public static final int MIN_RANGE = 30;
    public  static final int ROUTE_RANGE = 100;
    public static boolean SET_FINISH = false;

    public static String getFormatoFecha(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
        Date newDate = new Date();
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("dd-MM-yyyy");
        String newDateString = spf.format(newDate);

        return newDateString;
    }

    public static String getFechaHoyCompleta() {
        //date output format
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    public static String getFechaHoyFormato() {
        //date output format
        String fecha = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String fc = dateFormat.format(cal.getTime());

        String[] mifc = fc.split("-");
        String mes = "";
        switch (mifc[1]){
            case "01":
                mes = "Enero";
                break;
            case "02":
                mes = "Febrero";
                break;
            case "03":
                mes = "Marzo";
                break;
            case "04":
                mes = "Abril";
                break;
            case "05":
                mes = "Mayo";
                break;
            case "06":
                mes = "Junio";
                break;
            case "07":
                mes = "Julio";
                break;
            case "08":
                mes = "Agosto";
                break;
            case "09":
                mes = "Septiembre";
                break;
            case "10":
                mes = "Octubre";
                break;
            case "11":
                mes = "Noviembre";
                break;
            case "12":
                mes = "Diciembre";
                break;
        }

        fecha = mifc[0] +" de "+mes+" del "+mifc[2];
        return fecha;
    }

    public static String getHoraCompleta() {
        //date output format
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    public static String getSaludoDia() {
        String saludo = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH");
        Calendar cal = Calendar.getInstance();
        int h =  Integer.parseInt(dateFormat.format(cal.getTime()));
        if(h > 5 && h < 12){
            saludo = "Buenos días!\n";
        }else if(h >= 12 && h < 21){
            saludo = "Buenas tardes!\n";
        }else{
            saludo = "Buenas noches\n";
        }

        return saludo;
    }

    public static String toUrl(String str){
        String strR = "";
        try {
            strR = URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            strR = "";
        }catch (Exception ex){
            strR = "";
        }
        return strR;
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isEnabledGPS(Context ctx){
        LocationManager manager = (LocationManager) ctx.getSystemService( Context.LOCATION_SERVICE );

        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    public static boolean networkConnectivity(Context _context) {
        ConnectivityManager cm = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static boolean validarRut(String rut) {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }



}
