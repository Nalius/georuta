package cl.boxapp.geoasistencia.classes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Nalius on 01-06-2017.
 */
public class DBhelper extends SQLiteOpenHelper {


    private static String DB = "georuta.db";
    private static int version = 1;

    public DBhelper(Context context) {
        super(context, DB, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        generarBD(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        generarBD(db);
    }

    public void generarBD(SQLiteDatabase db){
        try{
            db.execSQL("CREATE TABLE IF NOT EXISTS Usuario(id int,id_user int, rut varchar(12),name varchar(255), first_lastname varchar(255), second_lastname varchar(255),email varchar(255), active int, imei varchar(255),id_office varchar(4))");
            Log.e("SQL","Tabla Usuario creada");
            db.execSQL("CREATE TABLE IF NOT EXISTS Empresas(id int, rut varchar(255), razon_social varchar(255))");
            Log.e("SQL","Tabla Empresas creada");

        }catch (SQLiteException ex){
            Log.e("ERRORSQL",ex.getMessage());
        }
    }

}

