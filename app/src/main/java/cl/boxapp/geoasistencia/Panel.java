package cl.boxapp.geoasistencia;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ShareCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cl.boxapp.geoasistencia.classes.Auxliar;
import cl.boxapp.geoasistencia.classes.GeoLocation;

public class Panel extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView TxtUbicacion;
    TextView txtnombreusuario;
    TextView txtxcorreousuario;
    TextView txtrutusuario;
    LocationManager Lm;
    Location Locat;
    String LatLong;
    boolean isGPSEnabled =false;
    boolean isNetworkEnabled =false;
    boolean canGetLocation =false;
    Location location;
    FragmentManager fm = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Mis Rutas");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //Log.e("AUXILIAR-PANEL",Auxliar.CURRENT_USER.getCorreo());

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        navigationView.post(new Runnable() {
            @Override
            public void run() {
                TxtUbicacion = (TextView) findViewById(R.id.TxtUbicacion);
                txtnombreusuario = (TextView) findViewById(R.id.txtnombreusuario);
                txtxcorreousuario = (TextView) findViewById(R.id.txtusuarioemail);
                txtrutusuario = (TextView) findViewById(R.id.txtusuariorut);
                txtxcorreousuario.setText(Auxliar.CURRENT_USER.getCorreo());
                txtnombreusuario.setText(Auxliar.CURRENT_USER.getNombres() +" "+Auxliar.CURRENT_USER.getApellido_Paterno());
                txtrutusuario.setText(Auxliar.CURRENT_USER.getRut());
                TxtUbicacion.setText("Ubic: "+GeoLocation.showLocation(GeoLocation.MyLocation,Panel.this));

                final Handler handler = new Handler(Looper.getMainLooper());
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        TxtUbicacion.setText("Ubic: "+GeoLocation.showLocation(GeoLocation.MyLocation,Panel.this));
                        handler.postDelayed(this, 5000); // Optional, to repeat the task.
                    }
                };
                handler.postDelayed(runnable, 5000);
            }
        });

        fm.beginTransaction().add(R.id.content_panel, new FrRutas()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.panel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                    //
                break;
            case R.id.about:
                Intent i = new Intent(Panel.this, AcercaDe.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action

        } else if (id == R.id.nav_salir) {
            new AlertDialog.Builder(Panel.this)
                    .setTitle("Confirmar")
                    .setMessage("¿Desea Salir")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            finish();
                        }})
                    .setNegativeButton(android.R.string.no, null).show();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
