package cl.boxapp.geoasistencia;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Text;

import java.util.ArrayList;

import cl.boxapp.geoasistencia.classes.Auxliar;
import cl.boxapp.geoasistencia.classes.GeoLocation;
import cl.boxapp.geoasistencia.classes.Ruta;

/**
 * Created by Nalius on 10-05-2017.
 */

public class ListRutas extends RecyclerView.Adapter<ListRutas.RutasViewHolder> {

    Context _context;
    ArrayList<Ruta> lista;


    public void setData(ArrayList<Ruta> lista){

                this.lista = lista;
                notifyDataSetChanged();

    }


    public ListRutas(ArrayList<Ruta> lista,Context context){


                this._context = context;
                this.lista = lista;

    }

    @Override
    public ListRutas.RutasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false);
        ListRutas.RutasViewHolder rvh = new ListRutas.RutasViewHolder(itemView);
        return rvh;
    }

    @Override
    public void onBindViewHolder(RutasViewHolder holder, int position) {
        Ruta ru = lista.get(position);
        holder.bindData(ru);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class RutasViewHolder extends RecyclerView.ViewHolder{

        ImageButton btnDetalleRuta;
        TextView txtNombreRuta;
        TextView txtFechaRuta;
        TextView txtdistanciaruta;

        public RutasViewHolder(View itemView) {
            super(itemView);
            //simpleSlidingDrawer =(SlidingDrawer) itemView.findViewById(R.id.simpleSlidingDrawer);
            btnDetalleRuta = (ImageButton) itemView.findViewById(R.id.btnDetalleRuta);
            txtNombreRuta = (TextView)itemView.findViewById(R.id.txtDetalleRuta);
            txtFechaRuta = (TextView)itemView.findViewById(R.id.txtFechaRuta);
            txtdistanciaruta = (TextView) itemView.findViewById(R.id.txtditanciaruta);
        }

        public void bindData(final Ruta ruta){
            GeoLocation.RutaLocation  = new Location("");
            GeoLocation.RutaLocation.setLatitude(ruta.getLatitud());
            GeoLocation.RutaLocation.setLongitude(ruta.getLongitud());
            Auxliar.CURRENT_ROUTE.setDescripcion(ruta.getDescripcion());
            Auxliar.CURRENT_ROUTE.setId(ruta.getId());
            Auxliar.CURRENT_ROUTE.setNombre_cliente(ruta.getNombre_cliente());
            Auxliar.CURRENT_ROUTE.setComentarios(ruta.getComentarios());
            Auxliar.CURRENT_ROUTE.setNombre_cliente(ruta.getNombre_cliente()+"");
//            boolean sw = false;
//            while (!sw){
//                if(GeoLocation.MyLocation!=null){
//                    sw = true;
                    float distanceInMeters = GeoLocation.MyLocation.distanceTo(GeoLocation.RutaLocation);
                    distanceInMeters = distanceInMeters / 1000;
                    txtdistanciaruta.setText("Dist. "+String.format("%.2f",distanceInMeters));
//               }
//            }

            btnDetalleRuta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(GeoLocation.MyLocation!=null){
                        Auxliar.SET_FINISH = false;
                        Intent i = new Intent(_context,DetalleRuta.class);
                        _context.startActivity(i);
                    }else{
                        Toast.makeText(_context,"Esperando GPS Intente Nuevamente",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            txtNombreRuta.setText(ruta.getDetalle());
            txtFechaRuta.setText(ruta.getFecha_hora());

        }
    }

}
