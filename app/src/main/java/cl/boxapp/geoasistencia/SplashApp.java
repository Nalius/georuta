package cl.boxapp.geoasistencia;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cl.boxapp.geoasistencia.classes.Auxliar;
import cl.boxapp.geoasistencia.classes.DBhelper;
import cl.boxapp.geoasistencia.classes.LocationService;
import cl.boxapp.geoasistencia.classes.Usuario;

public class SplashApp extends Activity {

    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.

    String[] permissions= new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_app);
        Thread timer = new Thread() {
            public void run(){
                try{
                    sleep(2000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }catch(Exception e) {
                    e.printStackTrace();
                }
                finally
                {


                    DBhelper dbh = new DBhelper(getApplicationContext());

                    SQLiteDatabase bd = dbh.getWritableDatabase();

                    Cursor cursor = bd.rawQuery("select id_user,rut,name,first_lastname,email from Usuario", null);

                    Log.e("CURSOR",String.valueOf(cursor.getCount()));

                    if (cursor.moveToFirst()) {
                        Log.e("FILA",cursor.getString(0));


                        Auxliar.CURRENT_USER.setId(cursor.getString(0));
                        Auxliar.CURRENT_USER.setRut(cursor.getString(1));
                        Auxliar.CURRENT_USER.setNombres(cursor.getString(2));
                        Auxliar.CURRENT_USER.setApellido_Paterno(cursor.getString(3));
                        Auxliar.CURRENT_USER.setCorreo(cursor.getString(4));

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){

                            if(checkPermissions()){
                                Intent i = new Intent(SplashApp.this,Panel.class);
                                Intent ii = new Intent(getApplication(), LocationService.class);
                                getApplication().startService(ii);
                                startActivity(i);
                                finish();

                            }
                        }else{
                            Intent i = new Intent(SplashApp.this,Panel.class);
                            Intent ii = new Intent(getApplication(), LocationService.class);
                            getApplication().startService(ii);
                            startActivity(i);
                            finish();
                        }
                    }else{
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){

                            if(checkPermissions()){
                                Intent i = new Intent(SplashApp.this,Registro.class);
                                Intent ii = new Intent(getApplication(), LocationService.class);
                                getApplication().startService(ii);
                                startActivity(i);
                                finish();

                            }
                        }else{
                            Intent i = new Intent(SplashApp.this,Registro.class);
                            Intent ii = new Intent(getApplication(), LocationService.class);
                            getApplication().startService(ii);
                            startActivity(i);
                            finish();
                        }
                       Log.e("ERROR","imBad :(");
                    }

                    bd.close();
                }
            }

        };
        timer.start();
  }

    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(getApplicationContext(),p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent ii = new Intent(getApplication(), LocationService.class);
                    Intent i = new Intent(SplashApp.this,Registro.class);
                    getApplication().startService(ii);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),"Debes aceptar todos los permisos para usar la app",Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }






}
