package cl.boxapp.geoasistencia;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cl.boxapp.geoasistencia.classes.Auxliar;
import cl.boxapp.geoasistencia.classes.GeoLocation;
import cl.boxapp.geoasistencia.classes.PathJSONParser;
import cl.boxapp.geoasistencia.classes.Ruta;

public class DetalleRuta extends FragmentActivity implements OnMapReadyCallback {
    ImageButton btnTomarFoto;
    ImageButton btnAddComentario;
    ImageButton btnDetalleRuta;
    ImageButton btnAnexos;
    FloatingActionButton btnConfirmarRuta;
    private GoogleMap googleMap;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    TextView txtdistancia;
    private static DetalleRuta ins;
    AlertDialog ad;

    final String TAG = "PathGoogleMapActivity";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ins = this;

        setContentView(R.layout.activity_detalle_ruta);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        txtdistancia = (TextView) findViewById(R.id.txtdistancia);
        btnTomarFoto = (ImageButton) findViewById(R.id.btnTomarFoto);
        btnAddComentario = (ImageButton) findViewById(R.id.btnAddComentario);
        btnDetalleRuta = (ImageButton) findViewById(R.id.btnDetalleRuta);
        btnConfirmarRuta = (FloatingActionButton) findViewById(R.id.btnConfirmarRuta);
        btnAnexos = (ImageButton) findViewById(R.id.btnDescargarAnexos);


        btnAnexos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Auxliar.networkConnectivity(DetalleRuta.this)){
                    new AsyncConsultaAnexos().execute(Auxliar.CURRENT_ROUTE);
                }else{
                    Toast.makeText(DetalleRuta.this,"Sin conexion a internet",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (i.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(i, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        btnAddComentario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogComment().show();
            }
        });

        btnDetalleRuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogDetail().show();
            }
        });

        btnConfirmarRuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(DetalleRuta.this)
                        .setTitle("Confirmar Ruta")
                        .setMessage("¿Desea dar ruta como cumplida?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                float distanceInMeters = GeoLocation.MyLocation.distanceTo(GeoLocation.RutaLocation);
                                distanceInMeters = distanceInMeters / 1000;

                                if (distanceInMeters <= Auxliar.MIN_RANGE) {
                                    if (Auxliar.networkConnectivity(DetalleRuta.this)){
                                        Log.e("RUTA",Auxliar.CURRENT_ROUTE.getId()+"");
                                        new AsyncFinalizarRuta().execute(Auxliar.CURRENT_ROUTE);
                                    }else{
                                        Toast.makeText(DetalleRuta.this,"Sin Conexion a internet",Toast.LENGTH_LONG).show();
                                    }
                                } else {

                                    Toast.makeText(getApplicationContext(), "Estas a"+String.format("%.2f",distanceInMeters)+ "de tu destino", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        setMarkers(origin, dest);

        return url;
    }

    public void setMarkers(LatLng origen, LatLng destino) {
        //googleMap.addMarker(new MarkerOptions().position(origen));
        googleMap.addMarker(new MarkerOptions().position(destino));
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            Log.e("Tamaño", result.size() + "");
            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions

                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);
            }

            if (lineOptions != null) {
                googleMap.addPolyline(lineOptions);
            }

        }
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //do nothing
            return;
        }
        googleMap.setMyLocationEnabled(true);
        Log.e("MyLatitud",String.valueOf(GeoLocation.MyLocation.getLatitude()));
        Log.e("MyLongitud",String.valueOf(GeoLocation.MyLocation.getLongitude()));
        Log.e("RutaLatitud",String.valueOf(GeoLocation.RutaLocation.getLatitude()));
        Log.e("RutaLongitud",String.valueOf(GeoLocation.RutaLocation.getLongitude()));
        String url = getDirectionsUrl(new LatLng(GeoLocation.MyLocation.getLatitude(), GeoLocation.MyLocation.getLongitude()),new LatLng(GeoLocation.RutaLocation.getLatitude(), GeoLocation.RutaLocation.getLongitude()));
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(GeoLocation.RutaLocation.getLatitude(), GeoLocation.RutaLocation.getLongitude()),
                13));

        float distanceInMeters = GeoLocation.MyLocation.distanceTo(GeoLocation.RutaLocation);

        distanceInMeters = distanceInMeters / 1000;

        txtdistancia.setText("Distancia (KM): "+String.format("%.2f",distanceInMeters));


        final Handler handler = new Handler(Looper.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                float distanceInMeters = GeoLocation.MyLocation.distanceTo(GeoLocation.RutaLocation);

                distanceInMeters = distanceInMeters / 1000;

                txtdistancia.setText("Distancia (KM): "+String.format("%.2f",distanceInMeters));
                Log.e("Repito",String.valueOf(distanceInMeters));

                googleMap.clear();


                String url = getDirectionsUrl(new LatLng(GeoLocation.MyLocation.getLatitude(), GeoLocation.MyLocation.getLongitude()),new LatLng(GeoLocation.RutaLocation.getLatitude(), GeoLocation.RutaLocation.getLongitude()));

                DownloadTask downloadTask = new DownloadTask();

                downloadTask.execute(url);

                handler.postDelayed(this, 5000); // Optional, to repeat the task.
            }
        };
        handler.postDelayed(runnable, 5000);

    }


    public Dialog DialogComment() {


        LayoutInflater layoutInflater = LayoutInflater.from(this);

        View promptView = layoutInflater.inflate(R.layout.dialog_layout, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();

        ImageButton btnAdd1 = (ImageButton) promptView.findViewById(R.id.btnok);

        ImageButton btnAdd2 = (ImageButton) promptView.findViewById(R.id.btnequis);

        EditText provText = (EditText) promptView.findViewById(R.id.editComentario);

        provText.setText(Auxliar.CURRENT_ROUTE.getComentarios());

        final EditText input = (EditText) promptView.findViewById(R.id.editComentario);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(DetalleRuta.this)
                        .setTitle("Confirmar Ruta")
                        .setMessage("¿Desea enviar comentario?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                if(Auxliar.networkConnectivity(DetalleRuta.this)){
                                    Auxliar.CURRENT_ROUTE.setComentarios(input.getText().toString());
                                    new AsyncComentarRuta().execute(Auxliar.CURRENT_ROUTE);
                                }else{
                                    Toast.makeText(DetalleRuta.this,"Sin conexion a internet",Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertD.hide();
            }
        });

         alertD.setView(promptView);

         ad = alertD;

        return alertD;
    }


    public Dialog DialogDetail() {

        LayoutInflater layoutInflater = LayoutInflater.from(this);

        View promptView = layoutInflater.inflate(R.layout.layout_detalle_ruta, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();

        TextView txtdetail = (TextView)  promptView.findViewById(R.id.txtdetalleruta);
        ImageButton btnAdd2 = (ImageButton) promptView.findViewById(R.id.btnequis);

        txtdetail.setText(Auxliar.CURRENT_ROUTE.getDescripcion());


        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertD.hide();
            }
        });

        alertD.setView(promptView);

        return alertD;
    }


    public class AsyncFinalizarRuta extends AsyncTask<Ruta,Void,JSONObject> {
        String url = Auxliar._SERVER;
        ProgressDialog pd;
        Boolean done = false;

        @Override
        protected void onPreExecute() {
            pd = ProgressDialog.show(DetalleRuta.this,"Ruta","Finalizando Ruta...");
        }

        @Override
        protected JSONObject doInBackground(Ruta... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPut httpPost = new HttpPut(url+"tracks/"+params[0].getId()+"/finish");
            JSONObject jsonObject = null;
            try {
                HttpResponse response = httpclient.execute(httpPost);

                if(response!=null){
                    String result = EntityUtils.toString(response.getEntity());

                    jsonObject = new JSONObject(result);
                    Log.e("RESULT",jsonObject.toString());

                    done = jsonObject.getBoolean("success");
                }

            } catch (ClientProtocolException e) {
                Log.e("ClientProtocolException",e.getMessage());

            } catch (IOException e) {
                Log.e("IOException",e.getMessage());
            } catch (JSONException e) {
                Log.e("JSONException",e.getMessage());
            }

            return jsonObject;

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            Auxliar.SET_FINISH = true;
            if(jsonObject!=null ){
                if(done){
                    Toast.makeText(DetalleRuta.this, "Ruta Finalizada", Toast.LENGTH_SHORT).show();
                    btnConfirmarRuta.setVisibility(View.INVISIBLE);
                    Intent i = new Intent(DetalleRuta.this,Panel.class);
                    startActivity(i);

                }
            }else{
                pd.dismiss();
                Toast.makeText(DetalleRuta.this,"Error al finalizar Ruta",Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }

    public class AsyncComentarRuta extends AsyncTask<Ruta,Void,JSONObject> {
        String url = Auxliar._SERVER;
        ProgressDialog pd;
        Boolean done = false;

        @Override
        protected void onPreExecute() {
            pd = ProgressDialog.show(DetalleRuta.this,"Ruta","Enviando comentario de uta...");
        }

        @Override
        protected JSONObject doInBackground(Ruta... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPut httpPost = new HttpPut(url+"tracks/"+params[0].getId());
            JSONObject jsonObject = null;
            JSONObject jsonPut = null;
            try {

                String json = "";
                jsonPut = new JSONObject();
                jsonPut.put("comment", params[0].getComentarios());
                json = jsonPut.toString();
                Log.e("Json",json);
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                HttpResponse response = httpclient.execute(httpPost);

                if(response!=null){
                    String result = EntityUtils.toString(response.getEntity());

                    jsonObject = new JSONObject(result);
                    Log.e("JSON",jsonObject.toString());

                    done = jsonObject.getBoolean("success");
                }

            } catch (ClientProtocolException e) {
                Log.e("ClientProtocolException",e.getMessage());

            } catch (IOException e) {
                Log.e("IOException",e.getMessage());
            } catch (JSONException e) {
                Log.e("JSONException",e.getMessage());
            }

            return jsonObject;

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(jsonObject!=null ){
                if(done){
                    Toast.makeText(DetalleRuta.this, "Comentario Enviado", Toast.LENGTH_SHORT).show();
                }
                ad.hide();
            }else{
                pd.dismiss();
                Toast.makeText(DetalleRuta.this,"Error al enviar comentario",Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }

    public class AsyncConsultaAnexos extends AsyncTask<Ruta,Void,JSONObject> {
        String url = Auxliar._SERVER;
        ProgressDialog pd;
        Boolean done = false;

        @Override
        protected void onPreExecute() {
            pd = ProgressDialog.show(DetalleRuta.this,"Ruta","Buscando...");
        }

        @Override
        protected JSONObject doInBackground(Ruta... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPut httpPost = new HttpPut(url+"tracks/"+params[0].getId()+"/attachments/");
            JSONObject jsonObject = null;
            JSONObject jsonPut = null;
            try {

                HttpResponse response = httpclient.execute(httpPost);

                if(response!=null){
                    String result = EntityUtils.toString(response.getEntity());

                    jsonObject = new JSONObject(result);
                    Log.e("JSON",jsonObject.toString());

                    done = jsonObject.getBoolean("success");

                    if(done){
                        String a = jsonObject.getString("file");
                        descargaArchivo(a);
                    }
                }

            } catch (ClientProtocolException e) {
                Log.e("ClientProtocolException",e.getMessage());

            } catch (IOException e) {
                Log.e("IOException",e.getMessage());
            } catch (JSONException e) {
                Log.e("JSONException",e.getMessage());
            }

            return jsonObject;

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(jsonObject!=null ){
                if(done){
                    pd.dismiss();
                    Toast.makeText(DetalleRuta.this,"Descargado",Toast.LENGTH_LONG).show();
                }
            }else{
                pd.dismiss();
                Toast.makeText(DetalleRuta.this,"Sin anexos",Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }

    public void descargaArchivo(String a){
        try {
            File root = getFilesDir();

            File dir = new File (String.valueOf(root));

            if(dir.exists()==false) {
                dir.mkdirs();
            }
            //indico URL al archivo
            String DownloadUrl= a;
            //nombre que tendrá el archivo
            String fileName = Auxliar.CURRENT_ROUTE.getNombre_cliente()+".pdf";

            URL url = new URL(DownloadUrl);
            File file = new File(dir, fileName);

            long startTime = System.currentTimeMillis();


            URLConnection ucon = url.openConnection();
            Log.d("Descarga", "CONEXION CORRECTA");
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            Log.d("Descarga", "INPUT STREAM");

            ByteArrayBuffer baf = new ByteArrayBuffer(5000);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baf.toByteArray());
            fos.flush();
            fos.close();
            Log.d("Descarga", "descarga lista en  + ((System.currentTimeMillis() - startTime) / 1000)" + "segundos");

        } catch (IOException e) {
            Log.d("Descarga", "Error: " + e);
        }
    }

}

